import uvicorn
from fastapi import FastAPI, Depends
from config import load_conf
from fastapi.security import HTTPBearer
from routes.login import login
from routes.register import register
from routes.checksession import session_active
from routes.user import *
from routes.models import *

app = FastAPI()
oauth2_scheme = HTTPBearer()


@app.get("/")
async def main_route():
    return {"message": "Nothing to do.", "status": 0}


@app.post("/login", response_model=LoginOutputSchema)
async def login_route(login_data: AuthSchema):
    return await login(username=login_data.username,
                       password=login_data.password)


@app.post("/register", response_model=ResponseStatus)
async def register_route(register_data: RegisterModel):
    return await register(register_data)


@app.post("/session", response_model=SessionSchema)
async def session_check(token=Depends(oauth2_scheme)):
    return session_active(token=token.credentials)


@app.get("/user/{id}/profile", response_model=ProfileModel)
async def get_profile_route(id: int):
    return get_profile(id=id)


@app.put("/user/{id}/profile", response_model=ResponseStatus)
async def edit_profile_route(id: int, data: ProfileModel, token=Depends(oauth2_scheme)):
    return edit_profile(token=token.credentials, id=id, data=data)


@app.delete("/user/{id}", response_model=ResponseStatus)
async def delete_profile_route(id: int, token=Depends(oauth2_scheme)):
    return delete_user(token=token.credentials, id=id)


@app.put("/user/{id}/change-password", response_model=ResponseStatus)
async def change_password_route(id: int, data: PasswordModel, token=Depends(oauth2_scheme)):
    return change_password(id=id, token=token.credentials, data=data)


@app.get("/is-authorized", response_model=AuthorizedModel)
async def authorized_checker(action: int, token=Depends(oauth2_scheme)):
    return is_authorized(action_code=action, token=token.credentials)


if __name__ == "__main__":
    config = load_conf()
    uvicorn.run(app, **config["server"])
