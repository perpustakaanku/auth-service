from sqlalchemy import create_engine, Column, Integer, String, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from config import load_conf
from fastapi import HTTPException, status
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

Base = declarative_base()


class ProfileTable(Base):
    __tablename__ = "profile"
    id = Column(Integer, primary_key=True, index=True)
    userid = Column(Integer, unique=True)
    name = Column(String)
    birthday = Column(Date)
    email = Column(String)


class Profile:
    def __init__(self):
        config = load_conf()["database"]

        self.__eng = create_engine(
            f'mysql+mysqlconnector://{config["username"]}:{config["password"]}@{config["host"]}:{config["port"]}/{config["name"]}'
        )

        self.__session = sessionmaker(bind=self.__eng)()

    def get_profile_by_userid(self, userid, **kwargs):
        result_obj = self.__session.query(ProfileTable).filter(ProfileTable.userid == userid)
        result = []

        for i in result_obj:
            result.append(i)

        if len(result) > 0:
            return result[0].__dict__
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="User not found")

    def add_profile(self, **data):
        self.__session.add(ProfileTable(**data))
        self.__session.commit()

        return True

    def edit_profile(self, userid, **data):
        try:
            record = self.__session.query(ProfileTable).filter(ProfileTable.userid == userid).one()

            for i in data:
                setattr(record, i, data[i])

            self.__session.commit()

            return True
        except NoResultFound:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="User not found")
        except MultipleResultsFound:
            return False

    def delete_profile(self, id, **kwargs):
        try:
            record = self.__session.query(ProfileTable).filter(ProfileTable.userid == id).one()
            self.__session.delete(record)
            self.__session.commit()

            return True
        except NoResultFound:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="User not found")
        except MultipleResultsFound:
            return False

    def __del__(self):
        self.__session.close()
        self.__eng.dispose()
