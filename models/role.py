from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from config import load_conf
from passlib.context import CryptContext
from fastapi import HTTPException, status
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

Base = declarative_base()


class RoleTable(Base):
    __tablename__ = "role"
    userid = Column(Integer, primary_key=True)
    role = Column(Integer)


class Role:
    def __init__(self):
        config = load_conf()["database"]

        self.__eng = create_engine(
            f'mysql+mysqlconnector://{config["username"]}:{config["password"]}@{config["host"]}:{config["port"]}/{config["name"]}'
        )

        self.__session = sessionmaker(bind=self.__eng)()
        self.__pwdcontext = CryptContext(schemes=["bcrypt"])

    def get_role_by_id(self, id):
        result_obj = self.__session.query(RoleTable).filter(RoleTable.userid == id)
        result = []

        for i in result_obj:
            result.append(i)

        if len(result) > 0:
            return result[0].__dict__["role"]
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="User not found")

    def add_role(self, id, role):
        data = {
            "userid": id,
            "role": role
        }

        self.__session.add(RoleTable(**data))
        self.__session.commit()

        return True

    def edit_role(self, id, **data):
        try:
            record = self.__session.query(RoleTable).filter(RoleTable.userid == id).one()

            for i in data:
                setattr(record, i, data[i])

            self.__session.commit()

            return True
        except NoResultFound:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="User not found")
        except MultipleResultsFound:
            return False

    def delete_role(self, id):
        try:
            record = self.__session.query(RoleTable).filter(RoleTable.userid == id).one()
            self.__session.delete(record)
            self.__session.commit()

            return True
        except NoResultFound:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="User not found")
        except MultipleResultsFound:
            return False

    def __del__(self):
        self.__session.close()
        self.__eng.dispose()
