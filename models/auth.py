from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from config import load_conf
from passlib.context import CryptContext
from fastapi import HTTPException, status
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from passlib.exc import UnknownHashError

Base = declarative_base()


class AuthTable(Base):
    __tablename__ = "auth"
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True)
    password = Column(String)


class Auth:
    def __init__(self):
        config = load_conf()["database"]

        self.__eng = create_engine(
            f'mysql+mysqlconnector://{config["username"]}:{config["password"]}@{config["host"]}' +
            f':{config["port"]}/{config["name"]}'
        )

        self.__session = sessionmaker(bind=self.__eng)()
        self.__pwdcontext = CryptContext(schemes=["bcrypt"])

    def __get_user(self, columns, value):
        result_obj = self.__session.query(AuthTable).filter(getattr(AuthTable, columns) == value)
        result = []

        for i in result_obj:
            result.append(i)

        if len(result) > 0:
            return result[0].__dict__
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="User not found")

    def get_user_by_id(self, id):
        return self.__get_user("id", id)

    def get_user_by_username(self, username):
        return self.__get_user("username", username)

    def check_password(self, username, password):
        try:
            user = self.get_user_by_username(username)
            return self.__pwdcontext.verify(password, user["password"])
        except UnknownHashError:
            return False

    def add_user(self, username, password):
        hashed_pass = self.__pwdcontext.hash(password)
        data = {
            "username": username,
            "password": hashed_pass
        }

        self.__session.add(AuthTable(**data))
        self.__session.commit()

        return True

    def edit_user(self, id, **data):
        try:
            record = self.__session.query(AuthTable).filter(AuthTable.id == id).one()

            for i in data:
                setattr(record, i, data[i])

            self.__session.commit()

            return True
        except NoResultFound:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="User not found")
        except MultipleResultsFound:
            return False

    def delete_user(self, id):
        try:
            record = self.__session.query(AuthTable).filter(AuthTable.id == id).one()
            self.__session.delete(record)
            self.__session.commit()
        except NoResultFound:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="User not found")
        except MultipleResultsFound:
            return False

    def __del__(self):
        self.__session.close()
        self.__eng.dispose()
