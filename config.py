import json
from functools import lru_cache


@lru_cache()
def load_conf():
    f = open("config.json", "r")
    res = json.load(f)
    f.close()

    return res