from jose import jwt, JWTError
from fastapi import HTTPException, status
from datetime import datetime
from constant import *
from routes.models import *


def get_session(token):
    cridentials_exeption = HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                                         detail="Could not validate cridentials",
                                         headers={"WWW-Authenticate": "Bearer"})

    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        userid = payload.get("userid")
        expired = payload.get("expired")

        if not userid:
            raise cridentials_exeption

        if datetime.timestamp(datetime.utcnow()) > expired:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                                detail="Token Expired",
                                headers={"WWW-Authenticate": "Bearer"})
        else:
            return {"userid": userid}

    except JWTError:
        raise cridentials_exeption


def check_session(**data):
    return get_session(data["token"])


def session_active(**data):
    result = check_session(**data)
    if result:
        return ResponseStatus(status=True)
    else:
        return ResponseStatus(status=False)
