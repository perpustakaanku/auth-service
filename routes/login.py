from datetime import timedelta, datetime
from fastapi import HTTPException, status
from models.auth import Auth
from jose import jwt
from constant import *
from routes.models import *


def create_token(**data):
    userid = data["userid"]
    expr = datetime.utcnow() + timedelta(days=EXPIRE_DAYS)

    data = {
        "userid": userid,
        "expired": datetime.timestamp(expr)
    }

    return jwt.encode(data, SECRET_KEY, algorithm=ALGORITHM)


async def login(**data):
    auth = Auth()

    if auth.check_password(data["username"], data["password"]):
        userid = auth.get_user_by_username(username=data["username"])["id"]
        token = create_token(userid=userid)
        return LoginOutputSchema(token=token)
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Username and Password combination not found")
