from models.auth import Auth
from models.profile import Profile
from models.role import Role
from fastapi import HTTPException, status
from routes.models import *


async def register(data: RegisterModel):
    if data.password == data.verifyPassword:
        auth = Auth()
        auth.add_user(username=data.username, password=data.password)

        userid = auth.get_user_by_username(username=data.username)["id"]

        Profile().add_profile(**{
            "userid": userid,
            "name": data.name,
            "birthday": data.birthday,
            "email": data.email
        })

        Role().add_role(userid, role=data.role)

        return ResponseStatus(status=True)
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Password and password verify mismatch")
