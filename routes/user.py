from models.role import Role
from models.auth import Auth
from models.profile import Profile
from fastapi import HTTPException, status
from routes.checksession import check_session
from routes.models import *

# CONSTANT
ADMIN_ROLE = 0b1000
DELETE_ROLE = 0b0100
CREATE_ROLE = 0b0010
EDIT_ROLE = 0b0001


def get_profile(id: int):
    return Profile().get_profile_by_userid(id)


def edit_profile(*, token, id, data: ProfileModel):
    session_id = check_session(token=token)["userid"]

    if id == session_id or (Role().get_role_by_id(id=session_id) | ADMIN_ROLE > 0):
        profile = Profile()
        profile.edit_profile(id, **{
            "name": data.name,
            "birthday": data.birthday,
            "email": data.email
        })

        return ResponseStatus(status=True)
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Unauthorized Action")


def delete_user(*, token, id):
    session_id = check_session(token=token)["userid"]

    if id == session_id or (Role().get_role_by_id(id=session_id) | ADMIN_ROLE > 0):
        Profile().delete_profile(id=id)
        Role().delete_role(id=id)
        Auth().delete_user(id=id)

        return ResponseStatus(status=True)
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Unauthorized Action")


def change_password(*, token, id, data: PasswordModel):
    session_id = check_session(token=token)["userid"]

    if id == session_id and data.password == data.verifyPassword:
        Auth().edit_user(id=id, **{
            "password": data.password
        })

        return ResponseStatus(status=True)
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Unauthorized Action")


def is_authorized(*, token, action_code):
    session_id = check_session(token=token)["userid"]

    if session_id:
        role = Role().get_role_by_id(session_id)

        if (role & action_code) > 0:
            return AuthorizedModel(result=True)
        else:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                                detail="Unauthorized Action")
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Unauthorized Action")
