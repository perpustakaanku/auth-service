from pydantic import BaseModel
from datetime import date


class ResponseToken(BaseModel):
    token: str


class AuthSchema(BaseModel):
    username: str
    password: str


class LoginOutputSchema(ResponseToken):
    pass


class SessionSchema(BaseModel):
    token: str


class ResponseStatus(BaseModel):
    status: bool


class ProfileModel(BaseModel):
    name: str
    birthday: date
    email: str


class PasswordModel(BaseModel):
    password: str
    verifyPassword: str


class RegisterModel(ProfileModel):
    username: str
    password: str
    verifyPassword: str
    role: int


class AuthorizedModel(BaseModel):
    result: bool
