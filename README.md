# Auth Service

Service ini mengurusi autentikasi user untuk menggunakan aplikasi managemen buku perpustakaan.

## Mengenai Aplikasi Perpustakaan

Aplikasi yang saya buat adalah aplikasi management perpustakaan. Pada aplikasi ini berisi beberapa fitur yang telah
diimplementasikan sebagai backend diantaranya adalah sebagai berikut:

* Akun pengguna
* Manajemen buku

## File Konfigurasi

Pada aplikasi ini terdapat file `config.json`. File ini berisikan konfigurasi server untuk menyambungkan ke database
atau service lain. Pada service ini, terdapat tiga konfigurasi penting, yaitu sebagai berikut:

### Server

Konfigurasi ini berisi mengenai bagaimana service ini akan dijalankan.

* Nilai `host` diisikan dengan alamat IP yang akan diattach sebagai server
* Nilai `port` adalah nilai port yang digunakan.

### Database

Konfigurasi ini berisi mengenai lokasi dan juga autentikasi database mysql yang akan digunakan.

## Cara Menjalankan Service

Berikut ini adalah langkah untuk menggunakan aplikasi ini:

1. Import database pada folder `./docs/database.sql`.
2. Aturlah `./config.json` sesuai dengan konfigurasi server yang ada pakai.
3. Pastikan anda telah mengunduh Python terbaru.
4. Buatlah lingkungan virtual Python jika dibutuhkan
5. Install _depedencies_ dari service ini. Gunakan :

```shell
pip install -r requirement.txt
```

4. Jalankan perintah erikut untuk memulai service ini:

```shell
python main.py
```

## ER Diagram

Berikut ini adalah desain database yang digunakan:
![ER Diagram](docs/auth-diagram.png)

## Service Endpoint

Penjelasan mengenai endpoint dapat anda lihat disini:
[https://documenter.getpostman.com/view/9498435/U16krR4j](https://documenter.getpostman.com/view/9498435/U16krR4j)